cmake_minimum_required(VERSION 2.8.5)

# Declare the version of HEP Tools we use
# (must be done before including heptools-common to allow evolution of the
# structure)
set(heptools_version  contrib)

include(${CMAKE_CURRENT_LIST_DIR}/heptools-common.cmake)

# please keep alphabetic order and the structure (tabbing).
# it makes it much easier to edit/read this file!

# Contribs
LCG_external_package(binutils          2.28                                     )
LCG_external_package(gcc              6.2.0                                     )
LCG_external_package(gcc              7.3.0                                     )
LCG_external_package(gcc              8.1.0                                     )
LCG_external_package(clang            6.0.0           gcc=7.3.0                 )
LCG_external_package(clang            7.0.0           gcc=8.1.0                 )

# Prepare the search paths according to the versions above
LCG_prepare_paths()
