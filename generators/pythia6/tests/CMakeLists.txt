cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
project(pythia6-tests CXX Fortran)

set(CMAKE_VERBOSE_MAKEFILE TRUE)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH2})

find_package(Pythia6 REQUIRED COMPONENTS pythia6_pdfdummy pythia6_dummy pythia6)
find_package(HepMC REQUIRED COMPONENTS HepMC fio)

find_package(FastJet REQUIRED)

include_directories(${CMAKE_SOURCE_DIR}/../../pythia8/tests/analyserhepmc ${PYTHIA6_INCLUDE_DIR} ${HEPMC_INCLUDE_DIR} ${FASTJET_INCLUDE_DIR})

set(TEST_DIR ${PYTHIA6_ROOT_DIR}/../share/sources/examples)

message(STATUS "HEPMC_LIBRARIES ${HEPMC_LIBRARIES}")
message(STATUS "PYTHIA6_LIBRARIES ${PYTHIA6_LIBRARIES}")
message(STATUS "FASTJET_LIBRARIES ${FASTJET_LIBRARIES}")

if(NATIVE_TESTS)
  add_executable(pythia6_orig_test1 ${TEST_DIR}/main78.cc)
  target_link_libraries(pythia6_orig_test1 ${PYTHIA6_LIBRARIES} ${HEPMC_LIBRARY} ${HEPMC_fio_LIBRARY})
endif(NATIVE_TESTS)

if(GENSER_TESTS)
  add_executable(pythia6_genser_test1 pythia6_genser_test1.cc ../../pythia8/tests/analyserhepmc/AnalyserHepMC.cc ../../pythia8/tests/analyserhepmc/LeptonAnalyserHepMC.cc ../../pythia8/tests/analyserhepmc/JetInputHepMC.cc)
  target_link_libraries(pythia6_genser_test1 ${PYTHIA6_LIBRARIES} ${HEPMC_LIBRARIES} ${HEPMC_fio_LIBRARY} ${FASTJET_LIBRARIES} ${PYTHIA6_ROOT_DIR}/lib//pydata.o)

  add_executable(pythia6_genser_outhepmc pythia6_genser_outhepmc.cc)
  target_link_libraries(pythia6_genser_outhepmc ${PYTHIA6_LIBRARIES} ${HEPMC_LIBRARIES} ${HEPMC_fio_LIBRARY} ${PYTHIA6_ROOT_DIR}/lib/pydata.o)
endif(GENSER_TESTS)

