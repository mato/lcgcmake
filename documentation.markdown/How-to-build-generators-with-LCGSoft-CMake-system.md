1. Clone GIT repository:

        git clone https://gitlab.cern.ch/sft/lcgcmake.git

2. Create build and install directories:

        mkdir lcgcmake-{build,install-opt}
        cd lcgcmake-build

3. Setup your environment (if needed)
    * `export FC=gfortran`  (actually this is needed only for slc5 platforms)
    * `export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/<VERSION>/Linux-x86_64/bin:$PATH`. Please use the same version as used by Jenkins. You can find it in [jk-setup.sh]( https://gitlab.cern.ch/sft/lcgjenkins/blob/master/jk-setup.sh).
    *  Setup compiler: either `source /cvmfs/sft.cern.ch/lcg/contrib/gcc/<GCC_VERSION>/<PLATFORM>/setup.sh` or `source /afs/cern.ch/sw/lcg/external/gcc/${GCC_VERSION}/<PLATFORM>/setup.sh`

4. Prepare make files using CMake:

        cmake -DLCG_INSTALL_PREFIX=/cvmfs/sft.cern.ch/lcg/releases -DLCG_VERSION=<release> -DCMAKE_INSTALL_PREFIX=../install -DPDFsets=minimal ../lcgcmake

    Advanced Example: building dev4 against latest nightly

        cmake -DLCG_INSTALL_PREFIX=/afs/cern.ch/sw/lcg/app/nightlies/dev4/$(date +%a) -DLCG_VERSION=dev4 -DCMAKE_INSTALL_PREFIX=../install -DPDFsets=minimal ../lcgcmake

    (for some explanations on the arguments see [Using LCGCMake](https://ph-dep-sft.web.cern.ch/document/using-lcgcmake) and [LCGCMake options](https://gitlab.cern.ch/sft/lcgcmake/wikis/LCGCMake-options))
    Other possible interesting arguments are:

    *`-DLCG_TARBALL_INSTALL=ON`

    *`-DPDFsets=ct10`

5. Build the binaries:

        make -j 8 <package>              # Build all versions of <package>
        make -j 8 <package>-<version>    # Build only specified version

6. Do tests (notice: doesn't work on AFS)

        ctest -R <package>
